Para poder usar este documento se tiene que realizar lo siguiente:

1. El funcionamiento o parte principal depende del contenedor (en docker) de bastion que simula un      servidor, en este se ligan los demas servidores de cada una de las partes que se describiran a continuacion.

2. Se crean otros 3 contenedores, uno para Sales, Accounting y finalmente HR.

3. Se conectan mediante SSH, y para no batallar con las direcciones IP, se unen en una sola red que en este caso llamaremos n1.

4. Una vez conectadas (y probadas las conexiones) con un simple ping (si regresa 'pong' quiere decir que se logro) pasaremos al archivo .yml

5. El archivo se edita en este caso con nano, y se agregan las instrucciones de lo que se realizara una ves se lance ansible con playbook.

6. Una vez editado y sin fallas en la sintaxis, se lanza el comando ansible-playbook playbook.yml para ejecutarlo, si todo sale bien y sin errores (pueden salir algunos si algun contenedor no esta encendido), se realizara la tarea que se le dio al playbook.yml de manera correcta.

7. Si salio correctamente, se creara la carpeta alumnos en todos los servidores (contenedores) y dentro de igual manera, se crearan los archivos con los nombres de los integrantes del equipo.
